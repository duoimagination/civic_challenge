import { Meteor } from 'meteor/meteor'; //required for calling meteor methods
import { Mongo } from 'meteor/mongo';
import '/imports/modules/handler.js';
//The way meteor includes javascript is kinda silly. It just bundles everything together.
Meteor.startup(() => {
	var apiLog = new Mongo.Collection("apilog");
	//apiLog.remove({});
	//Initialize API log if empty
	if (apiLog.find().count() == 0) {
		console.log("Initializing Api log");

		var apiSize = 1000;
	  	for (var i=0; i<apiSize; i++) {
	  		//console.log(Meteor.call('genereateAPI', 'JohnSmith'+i));
	  		apiLog.insert (Meteor.call('genereateAPI', 'JohnSmith'+i));
	  	}
	} else {
		console.log("Api log initialized");
	}
});
