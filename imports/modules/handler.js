var endPoints = ["login", "signup", "payment", "search", "browse"];
var secondsInMonth = 2592000

var randInt = function (_min, _max) {
	return Math.floor(Math.random() * (_max - _min + 1)) + _min;
}

var randEndPoint = function  () {
	return endPoints[randInt(0, endPoints.length-1)]
}

Meteor.methods({
    genereateAPI: function(_clientName){
    	var tStamp = new Date().valueOf();
        return {
        	timestamp: randInt (tStamp, tStamp+secondsInMonth),
        	clientname: _clientName,
        	endpoint: randEndPoint(),
        	result: Boolean(randInt(0, 1)) ? "Success" : "Error" 
        }	
    },
    generateTimeStamp: function () {
        var tStamp = new Date().valueOf();
        return randInt (tStamp, tStamp+secondsInMonth);
    },
});