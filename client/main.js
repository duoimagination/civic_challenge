import { Template } from 'meteor/templating';
import './main.html';


var apiLog = new Mongo.Collection("apilog");

function getTime () {
	
}
/*****************************************************/
Template.apiList.helpers({
	apiCall() {
		//1 or -1 to specify an ascending or descending sort respectively.
		return apiLog.find({}, {sort: {timestamp: 1, createdAt: 1}, reactive:true});
	}
});

Template.newApiCall.events({
	'submit .new-api-call'(event) {
	    // Prevent default browser form submit
	    event.preventDefault();

	    // Get value from form element
	    const target = event.target;
	    
	    //Due to meteor's client async behavior
	    Meteor.call('generateTimeStamp', function(err, resp) {
			apiLog.insert({
	        	timestamp: resp,
	        	clientname: target.clientname.value,
	        	endpoint: target.endpoint.value,
	        	result: target.result.value
	        });
		});
	}
});

